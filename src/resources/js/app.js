require('./bootstrap');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function(){
    $("#addWidgetForm").hide();
    $(".skeleton .slot").hide();

    $("#showCreateForm").click(function(){

        $("#addWidgetForm").toggle();
        $(".skeleton .slot").toggle();
    });

});

var btn = document.querySelector('.add');
var remove = document.querySelector('.draggable');

function dragStart(e) {
    this.style.opacity = '0.4';
    dragSrcEl = this;
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', this.innerHTML);
};

function dragEnter(e) {
    this.classList.add('over');
}

function dragLeave(e) {
    e.stopPropagation();
    this.classList.remove('over');
}

function dragOver(e) {
    e.preventDefault();
    e.dataTransfer.dropEffect = 'move';
    return false;
}

function dragDrop(e) {
    if (dragSrcEl != this) {
        dragSrcEl.innerHTML = this.innerHTML;
        this.innerHTML = e.dataTransfer.getData('text/html');
    }
    return false;
}

function dragEnd(e) {
    var listItens = document.querySelectorAll('.draggable');
    [].forEach.call(listItens, function(item) {
        item.classList.remove('over');
    });
    this.style.opacity = '1';
    moveWidgets();

}

function addEventsDragAndDrop(el) {
    el.addEventListener('dragstart', dragStart, false);
    el.addEventListener('dragenter', dragEnter, false);
    el.addEventListener('dragover', dragOver, false);
    el.addEventListener('dragleave', dragLeave, false);
    el.addEventListener('drop', dragDrop, false);
    el.addEventListener('dragend', dragEnd, false);
}

var listItens = document.querySelectorAll('.draggable');
[].forEach.call(listItens, function(item) {
    addEventsDragAndDrop(item);
});

function addNewItem() {
    var newItem = document.querySelector('.input').value;
    if (newItem != '') {
        document.querySelector('.input').value = '';
        var li = document.createElement('li');
        var attr = document.createAttribute('draggable');
        var ul = document.querySelector('ul');
        li.className = 'draggable';
        attr.value = 'true';
        li.setAttributeNode(attr);
        li.appendChild(document.createTextNode(newItem));
        ul.appendChild(li);
        addEventsDragAndDrop(li);
    }
}

function moveWidgets() {
    let token   = $('meta[name="csrf-token"]').attr('content');
    let data = [];
    let $li =$('#widgets').children();
    $li.each(function (index) {
        data.push($(this).find("#widgetId").val());
    });
    console.log(data);
    $.ajax({
        type: "moveWidgets",
        url: "/ajaxHomepage",
        method: 'post',
        data: {
            _token: token,
            data: data
        },
        success: function (data) {
            console.log('success', data);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

function showHide() {
    e.preventDefault();
    let div = $('.card');
    if(div.css("display","none")){
        div.css("display","block")
    }
}
