<div class="skeleton" >
    <input id="widgetId" type="hidden" value="{{ $WidgetTemplate->id }}">
    <div class="slot">
        <ul class="staff">
            <li class="dim">{{ $WidgetTemplate->position }}</li>
            <li class="handle"><strong>{{ $WidgetTemplate->name }}</strong></li>
            <li><a class="delete-here" target="_blank" href="{{ URL::to('/').'/admin/widgets/'.$WidgetTemplate->id.'/edit' }}">Edit</a>
            </li>
            <li><a href="{{ url('/widgets/'.$WidgetTemplate->id ) }}" class="delete-here">Delete</a></li>
        </ul>
    </div>
</div>
@if (!is_null($WidgetTemplate->viewName))
    @include('WidgetViews::homepage.'.$WidgetTemplate->viewName)
@else
    <h6>There is no template with name : "{{ $WidgetTemplate->viewName }}"</h6>
@endif


