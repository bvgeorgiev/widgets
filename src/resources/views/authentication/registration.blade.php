@extends('voyager::auth.master')

@section('content')
    <div class="login-container">
        {{-- Registration Form --> --}}
        <form action="{{ route('createUser') }}" method="POST">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Enter your name</label>
                    <input type="name" name="name" class="form-control @if(isset($error['name'][0]))is-invalid @endif"
                           placeholder="Ivan Ivanov" value="@if(isset($name)) {{$name}} @endif">
                </div>
                <div class="form-group">
                    <label for="email">Enter Email</label>
                    <input type="email" name="email" class="form-control @if(isset($error['email'][0]))is-invalid @endif"
                           placeholder="email@email.com" value="@if(isset($email)){{ $email }} @endif">
                </div>
                <div class="form-group">
                    <label for="password">Enter Password</label>
                    <input type="password" name="password" class="form-control @if(isset($error['password'][0]))is-invalid @endif"
                           placeholder="password">
                </div>
                <div class="form-group">
                    <label for="password_confirm">Confirm Password</label>
                    <input type="password" name="password_confirm" class="form-control @if(isset($error['password'][0]))is-invalid @endif"
                           placeholder="password_confirm">
                </div>
                <div class="form-group">
                    <input type="submit" name="login" class="btn btn-primary" value="Create User">
                </div>

            </form>
        {{-- Registration Form Errors --> --}}
        @if(count($error)>0)
            <div class="alert alert-red">
                <ul class="list-unstyled">
                    @foreach($error as $err)
                        <li>{{ $err[0] }} </li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endsection

    @section('post_js')

    <script>
            var btn = document.querySelector('button[type="submit"]');
            var form = document.forms[0];
            var email = document.querySelector('[name="email"]');
            var password = document.querySelector('[name="password"]');
            btn.addEventListener('click', function(ev){
                if (form.checkValidity()) {
                    btn.querySelector('.signingin').className = 'signingin';
                    btn.querySelector('.signin').className = 'signin hidden';
                } else {
                    ev.preventDefault();
                }
            });
            email.focus();
            document.getElementById('emailGroup').classList.add("focused");

            // Focus events for email and password fields
            email.addEventListener('focusin', function(e){
                document.getElementById('emailGroup').classList.add("focused");
            });
            email.addEventListener('focusout', function(e){
                document.getElementById('emailGroup').classList.remove("focused");
            });

            password.addEventListener('focusin', function(e){
                document.getElementById('passwordGroup').classList.add("focused");
            });
            password.addEventListener('focusout', function(e){
                document.getElementById('passwordGroup').classList.remove("focused");
            });

        </script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

@endsection







