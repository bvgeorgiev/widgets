<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel Voyager</title>


        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="navbar navbar-inverse navbar-fixed-top">
            @auth
                    <a href="{{ url('/') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Logo</a>
                    <a href="{{ route('logout') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Logout</a>
            @else
                    <a href="{{ route('showLogin') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>
                    <a href="{{ route('showRegistrationForm') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
            @endauth
        </div>
        <div class="container">
            <div class="row justify-content-center">
                {{--    Get all Widgets adn render their templates   --}}
                <ul id="widgets">
                    @foreach($WidgetTemplates as $WidgetTemplate)
                        @php
                            $WidgetTemplateName =  $WidgetTemplate->viewName;
                            $WidgetTemplateId =  $WidgetTemplate->id;
                        @endphp
                        <li class="draggable" draggable="true">
                            @include('WidgetViews::widget',compact('WidgetTemplate'))
                        </li>
                    @endforeach
                </ul>

                {{-- Todo create the same slot as other widgets --}}
                <div class="col-md-10">
                    {{--    Form for creating a new Widget !!!        --}}
                    <form action="{{ route('widget.index.post') }}" id="addWidgetForm" method="POST">
                        @csrf
                        <div class="card">
                            <div class="form-group">
                                <label for="name">Widget Name</label>
                                <input type="text" class="form-control" id="name" name="name"/>
                            </div>
                            <div class="form-group">
                                <label for="widgets">Choose template:</label>

                                <select name="viewName" id="viewName">
                                    @foreach($widgets as $widget)
                                        @if($widget->viewName != null)
                                            <option value="{{ $widget->viewName }}">{{ $widget->viewName }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" id="route" name="route" value="{!! str_replace(URL::to('/'),'',url()->current()) !!}">
                            <div class="card-footer text-right">
                                <button type="submit" id="addWidgetButton" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{--  Todo:: Fix this and add better checking !!!   --}}
        @if(isset(Auth::user()->role_id))
            @if(Auth::user()->role_id == 1)
                <button id="showCreateForm" class="neu-btn">Widgets</button>
            @endif
        @endif

    </body>

    <!--     jquery CDN   -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</html>
