<div class="homePrograms">
        <img class="background" src="{{ asset('storage/'.$WidgetTemplate->backgroundImage)  }}" alt="background"/>
    <div class="programs">
        <div class="programsLeft">
            <span>FEATURED PROGRAM</span>
            <h2 class="programsTitle">{{ $WidgetTemplate->mainTitle }}</h2>
            <p class="programMainText">{{ $WidgetTemplate->mainText }}</p>
            <button class="exploreProgram">Explore Program</button>
        </div>
        <div class="programsRight">
            <span>OTHER FEATURED PROGRAM</span>
            @foreach($programs as $program)
                <div class="program">
                    <h2>{{ $program->name}}</h2>
                    <span>{{ $program->description }}</span>
                </div>
            @endforeach
        </div>
    </div>
</div>



