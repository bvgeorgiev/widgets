<div class="homeStudy">

    <div class="studies">
        <div class="studysLeft">
            <span>CAMPUSES</span>
            <h2 class="studyTitle">{{ $WidgetTemplate->mainTitle }}</h2>
            @foreach($programs as $program)
                <div class="study">
                    <h2>{{ $program->name}}</h2>
                </div>
            @endforeach
        </div>
        <div class="studysRight">
                <img class="background" src="{{ asset('storage/'.$WidgetTemplate->backgroundImage)  }}" alt="background"/>
        </div>
    </div>
</div>



