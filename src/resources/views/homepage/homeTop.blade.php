<div class="homeTop">
    <div class="darker"></div>
    <img class="background" src="{{ asset('storage/'.$WidgetTemplate->backgroundImage)  }}" alt="background"/>
    <h2>{{ $WidgetTemplate->mainTitle }}</h2>
    <div class="searchFilter">
        <img src="https://img.icons8.com/ios/50/000000/search--v3.png"/>
        <input type="text" placeholder="     What are you looking for?">
    </div>
    <div class="carousel">
        <div class="post">
            <h4>Future Students</h4>
            <h3>Reimagine your future.</h3>
        </div>
        <div class="post">
            <h4>Future Students</h4>
            <h3>Reimagine your future.</h3>
        </div>
        <div class="post">
            <h4>Future Students</h4>
            <h3>Reimagine your future.</h3>
        </div>
        <div class="post">
            <h4>Future Students</h4>
            <h3>Reimagine your future.</h3>
        </div>
    </div>
</div>



