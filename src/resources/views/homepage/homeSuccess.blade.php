<div class="homeSuccess">
    <img class="background" src="{{ asset('storage/'.$WidgetTemplate->backgroundImage)  }}" alt="background"/>
    <h6 class="secondaryText">{{ $WidgetTemplate->secondaryText}}</h6>
    <span class="widgetTitle">{{ $WidgetTemplate->mainTitle }}</span>
    <div>
        <span class="mainText">{{ $WidgetTemplate->mainText }}</span>
        <span class="sometext">{{ $WidgetTemplate->secondaryText}}</span>
        <button class="explore">Explore Program</button>
    </div>
</div>



