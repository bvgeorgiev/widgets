<?php

namespace Packages\Widgets;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
//use Packages\Widgets\App\Controllers\WidgetsController;

class WidgetsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            // Export the migration
            if (! class_exists('CreatePostsTable')) {
                $this->publishes([
                    __DIR__ . '/database/migrations/2021_11_19_1637314121_create_widgets_table.php' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_widgets_table.php'),
                    __DIR__ . '/database/migrations/2021_11_19_1637314238_create_templates_table.php' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_templates_table.php'),
                    // you can add any number of migrations here
                ], 'migrations');
            }
        }
//        php artisan vendor:publish --provider="JohnDoe\BlogPackage\BlogPackageServiceProvider" --tag="migrations"
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        View::addNamespace('WidgetViewsVoyager', __DIR__.'/resources/views/vendor/voyager/auth');
        View::addNamespace('WidgetViews', __DIR__.'/resources/views');
        $this->loadViewsFrom(__DIR__.'/resources/views',"WidgetViews");

    }
}
