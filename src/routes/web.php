<?php
namespace Packages\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Voyager;
use Packages\Widgets\App\Controllers\WidgetsController;
use Packages\Widgets\App\Controllers\AuthController;
use Packages\Widgets\App\Controllers\RegistrationController;
use App\Program;

////User Auth routes !!!
//Route::group(['prefix' => 'auth'], function () {
//    Route::get('login',[AuthController::class, 'showLogin'])->name('showLogin');
//    Route::get('checkLogin',[AuthController::class, 'checkLogin'])->name('checkLogin');
//    Route::post('authLogin',[AuthController::class, 'authLogin'])->name('authLogin');
//    Route::get('showRegistrationForm',[RegistrationController::class, 'showRegistrationForm'])->name('showRegistrationForm');
//    Route::post('registration',[RegistrationController::class, 'createUser'])->name('createUser');
//    Route::get('logout',[AuthController::class, 'logout'])->name('logout');
//});

// Widget routes !!!

Route::get('/', [WidgetsController::class, 'index'])->name('widget.index');
Route::post('/', [WidgetsController::class, 'createWidget'])->name('widget.index.post');
Route::get('/widgets/{id}', [WidgetsController::class, 'deleteWidget'])->name('widget.index.delete');
Route::post('/ajaxWidgets', [WidgetsController::class, 'moveWidget'])->name('widget.index.move');

// Programs routes !!!
Route::get('/programs', [AuthController::class, 'lele']);
