<?php

namespace Packages\Widgets\App\Controllers;

use App\Widget;
use App\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;


class WidgetsController
{

//    public function __construct(){
//        dump(get_class());
//    }
    public function index(Request $request)
    {
        $WidgetTemplates = [];
        $contentWidgets = DB::table('widgets')->where('route', '/homepage')->get()->toArray();
        // Sort by posistion
        $contentWidgets = $this->getSortedContentWidgets($contentWidgets);
        foreach ($contentWidgets as $contentWidget){
            $WidgetTemplates[] = $contentWidget;
        }

        $widgets = Widget::all();
        $programs = Program::all();
        return view('WidgetViews::homepage',compact('widgets','WidgetTemplates'),compact('programs','programs'));
    }

    /**
     * Create a widget
     * @param Request $request
     * @return void
     */
    public function createWidget(Request $request): \Illuminate\Http\RedirectResponse
    {
        $route = $this->getRoute($request);
        if($request->post('name') !== null) {
            Widget::create([
                'name' => $request->get('name'),
                'route' => $route,
                'position' => 100,
                'viewName' => $request->get('viewName'),
            ]);
        }
        $widget = DB::table('widgets')->where('name', $request->get('name'))->first();
        return redirect('/admin/widgets/'.$widget->id.'/edit');
    }

    /**
     * Delete widget by id
     * @param Int $id
     * @return void
     */
    public function deleteWidget($id): \Illuminate\Http\RedirectResponse
    {
        Widget::where('id', $id)->delete();
        return redirect()->route('widget.index.post');
    }


    /**
     * Move/Change position of a widget
     * @param Request $request
     * @return void
     */
    public function moveWidget(Request $request)
    {
        $widgetsOrder = $request->data;
        foreach ($widgetsOrder as $key=> $widgetOrder){
            $widget =  Widget::find($widgetOrder);
            $widget->position = $key+1;
            $widget->save();
        }

    }

    /**
     * @param array $contentWidgets
     * @return array
     */
    public function getSortedContentWidgets(array $contentWidgets): array
    {
        usort($contentWidgets, function ($item1, $item2) {
            return $item1->position <=> $item2->position;
        });
        return $contentWidgets;
    }


    /**
     * @param Request $request
     * @return mixed|string
     */
    public function getRoute(Request $request)
    {
        if ($request->get('route') != '' && $request->get('route') != null) {
            $route = $request->get('route');
        } else {
            $route = '/homepage';
        }
        return $route;
    }
}
