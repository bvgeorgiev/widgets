<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplatesTable extends Migration
{
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {

		$table->integer('id',)->unsigned();
		$table->timestamp('created_at')->nullable()->default('NULL');
		$table->timestamp('updated_at')->nullable()->default('NULL');
		$table->string('name')->nullable()->default('NULL');
		$table->json('fields')->nullable()->default('NULL');

        });
    }

    public function down()
    {
        Schema::dropIfExists('templates');
    }
}