<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetsTable extends Migration
{
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {

		$table->integer('id',)->unsigned();
		$table->timestamp('created_at')->nullable()->default('NULL');
		$table->timestamp('updated_at')->nullable()->default('NULL');
		$table->string('backgroundImage')->nullable()->default('NULL');
		$table->string('mainTitle')->nullable()->default('NULL');
		$table->string('secondaryText')->nullable()->default('NULL');
		$table->string('mainText')->nullable()->default('NULL');
		$table->string('route')->default('/homepage');
		$table->string('viewName')->nullable()->default('NULL');
		$table->integer('position',)->nullable()->default('NULL');
		$table->string('name')->nullable()->default('NULL');

        });
    }

    public function down()
    {
        Schema::dropIfExists('widgets');
    }
}